package aoc_utils

import (
	"reflect"
)

func Reverse[T any](slice []T) []T {
	reverse := []T{}

	for i := range slice {
		reverse = append(reverse, slice[len(slice)-i-1])
	}

	return reverse
}

func IndexOf[T comparable](slice []T, value T) int {
	for i, v := range slice {
		if reflect.DeepEqual(v, value) {
			return i
		}
	}

	return -1
}

func Find[T comparable](slice []T, predicate func(T) bool) (bool, T) {
	var e T

	for _, s := range slice {
		if predicate(s) {
			return true, s
		}
	}

	return false, e
}

func FindAll[T any](slice []T, predicate func(T) bool) []T {
	matches := []T{}

	for _, item := range slice {
		if predicate(item) {
			matches = append(matches, item)
		}
	}

	return matches
}

func RemoveAt[T any](slice []T, index int) []T {
	return append(slice[:index], slice[index+1:]...)
}

func Remove[T comparable](slice []T, item T) []T {
	index := IndexOf(slice, item)
	return RemoveAt(slice, index)
}

func StringContainsRune(input string, value rune) bool {
	for _, i := range input {
		if i == value {
			return true
		}
	}

	return false
}

func Contains[T comparable](slice []T, value T) bool {
	for _, v := range slice {
		if reflect.DeepEqual(v, value) {
			return true
		}
	}

	return false
}

func SumInt(slice []int) int {
	output := 0

	for _, s := range slice {
		output += s
	}

	return output
}

func Dequeue[T any](slice *[]T) T {
	item := (*slice)[0]

	if len(*slice) == 1 {
		*slice = []T{}
	} else {
		*slice = (*slice)[1:]
	}

	return item
}

func MaxIndex[T ~int](slice []T) int {
	maxIndex := 0

	for i, j := range slice {
		if j > slice[maxIndex] {
			maxIndex = i
		}
	}

	return maxIndex
}

func Pop[T any](slice []T) (T, []T) {
	item := slice[0]

	return item, slice[1:]
}
