package aoc_utils

import (
	"fmt"
)

func reconstructPath(current Coordinate, cameFrom map[string]Coordinate) []Coordinate {
	path := []Coordinate{current}

	for ok := true; ok; ok = ok {
		current, ok := cameFrom[current.ToString()]
		if ok {
			path = append([]Coordinate{current}, path...)
		}
	}

	return path
}

func CoordinateA_Star(start Coordinate, end Coordinate, nodes []Coordinate, next func([]Coordinate, Coordinate) []Coordinate, hueristic func(Coordinate, Coordinate) int, score func([]Coordinate, Coordinate) int) []Coordinate {
	openNodes := []Coordinate{}
	openNodes = append(openNodes, start)
	cameFrom := make(map[string]Coordinate)

	gScore := make(map[string]int)
	gScore[start.ToString()] = 0

	fScore := make(map[string]int)
	fScore[start.ToString()] = hueristic(start, end)

	potentialWins := make(map[string]int)
	winMin := MaxInt

	deadEndNodes := []Coordinate{}

	fmt.Println("Running A* for Coordinates...\n")

	for ok := len(openNodes) > 0; ok; ok = len(openNodes) > 0 {
		var current Coordinate
		highScore := MaxInt

		for _, o := range openNodes {
			if fScore[o.ToString()] < highScore {
				highScore = fScore[o.ToString()]
				current = o
			}
		}

		Remove(openNodes, current)

		if current == end {
			potentialWins[current.ToString()] = highScore
			if highScore < winMin {
				winMin = highScore
			}
			continue
		}

		if highScore > winMin {
			deadEndNodes = append(deadEndNodes, current)
		}

		if Contains(deadEndNodes, current) {
			continue
		}

		nextNodes := next(nodes, current)

		if len(nextNodes) < 1 {
			deadEndNodes = append(deadEndNodes, current)
			continue
		}

		for _, n := range nextNodes {
			g := gScore[current.ToString()] + score(nodes, n)
			p, e := gScore[n.ToString()]
			if !e || g < p {
				cameFrom[n.ToString()] = current
				gScore[n.ToString()] = g
				fScore[n.ToString()] = g + hueristic(n, end)
				openNodes = append(openNodes, n)
			}
		}
	}

	var winner Coordinate

	for k, v := range potentialWins {
		if v <= winMin {
			winner = NewCoordinate(k)
		}
	}

	return reconstructPath(winner, cameFrom)
}
