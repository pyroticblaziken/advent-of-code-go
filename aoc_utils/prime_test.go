package aoc_utils

// import (
// 	"math/big"
// 	"reflect"
// 	"testing"
// )

// type Int = big.Int

// func TestGetPrimeFactors(t *testing.T) {
// 	var tests = []struct {
// 		name  string
// 		value uint64
// 		want  map[Int]Int
// 	}{
// 		{"4", 4, map[Int]Int{2: 2}},
// 		{"19", 19, map[Int]Int{19: 1}},
// 		{"28576", 28576, map[Int]Int{2: 5, 19: 1, 47: 1}},
// 	}

// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			actual := GetPrimeFactors(tt.value)
// 			if !reflect.DeepEqual(actual, tt.want) {
// 				t.Errorf("GetPrimeFactors(%v) actual %v, expected: %v", tt.value, actual, tt.want)
// 			}
// 		})
// 	}
// }
