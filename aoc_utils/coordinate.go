package aoc_utils

import (
	"fmt"
	"strconv"
	"strings"
)

type Coordinate struct {
	X, Y, Z int
}

func (a *Coordinate) Add(b Coordinate) Coordinate {
	return Coordinate{X: a.X + b.X, Y: a.Y + b.Y, Z: a.Z + b.Z}
}

func (a *Coordinate) Subtract(b Coordinate) Coordinate {
	return Coordinate{X: a.X - b.X, Y: a.Y - b.Y, Z: a.Z + b.Z}
}

func (a *Coordinate) ManhattanDistance(b Coordinate) int {
	return AbsInt(a.X-b.X) + AbsInt(a.Y-b.Y)
}

func (a *Coordinate) ManhattanDistance2(b Coordinate) int {
	return AbsInt(a.X-b.X) + AbsInt(a.Y-b.Y) + AbsInt(a.Z-b.Z)
}

func (a *Coordinate) IsAdjacent(b Coordinate) bool {
	if AbsInt(a.X-b.X) > 1 {
		return false
	}

	if AbsInt(a.Y-b.Y) > 1 {
		return false
	}

	if AbsInt(a.Z-b.Z) > 1 {
		return false
	}

	return true
}

func (a *Coordinate) ToUnitVector() Coordinate {
	x, y, z := 0, 0, 0
	if a.X > 0 {
		x = 1
	} else if a.X < 0 {
		x = -1
	}

	if a.Y > 0 {
		y = 1
	} else if a.Y < 0 {
		y = -1
	}

	if a.Z > 0 {
		z = 1
	} else if a.Z < 0 {
		z = -1
	}

	return Coordinate{X: x, Y: y, Z: z}
}

func (a *Coordinate) ToString() string {
	return fmt.Sprintf("%v,%v,%v", a.X, a.Y, a.Z)
}

func NewCoordinate(s string) Coordinate {
	parts := strings.Split(s, ",")
	x, _ := strconv.Atoi(parts[0])
	y, _ := strconv.Atoi(parts[1])
	z, _ := strconv.Atoi(parts[2])

	return Coordinate{X: x, Y: y, Z: z}
}
