package aoc_utils

import (
	"bufio"
	"fmt"
	"os"
)

const MaxUint = ^uint(0)
const MinUint = 0
const MaxInt = int(MaxUint >> 1)
const MinInt = -MaxInt - 1

func ReadInputFile(filePath string) []string {
	output := []string{}

	readFile, err := os.Open(filePath)

	if err != nil {
		fmt.Println(err)
	}

	fileScanner := bufio.NewScanner(readFile)

	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		output = append(output, fileScanner.Text())
	}

	readFile.Close()

	return output
}

func AbsInt(n int) int {
	y := n >> 31
	return (n ^ y) - y
}
