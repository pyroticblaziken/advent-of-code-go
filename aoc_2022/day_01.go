package main

import (
	"strconv"

	"platinumleo.net/aoc_utils"
)

func CountCalories(input []string) []int {
	currCals := 0
	output := []int{}

	for _, l := range input {
		t, err := strconv.Atoi(l)

		if err != nil { // If we have a valid number
			output = append(output, currCals)
			currCals = 0
		} else {
			currCals += t
		}
	}

	if currCals > 0 {
		output = append(output, currCals)
	}

	return output
}

func MaxCalories(caloriesCarried []int) int {
	output := 0

	for _, c := range caloriesCarried {
		if output < c {
			output = c
		}
	}

	return output
}

func TopThreeCalories(caloriesCarried []int) []int {
	output := []int{}

	for ok := true; ok; ok = (len(output) < 3) {
		max := MaxCalories(caloriesCarried)
		output = append(output, max)
		index := aoc_utils.IndexOf(caloriesCarried, max)
		caloriesCarried = aoc_utils.RemoveAt(caloriesCarried, index)
	}

	return output
}

// main.go
// func main() {
// 	input := aoc_utils.ReadInputFile("data/day01.txt")

// 	calories := CountCalories(input)

// 	max := MaxCalories(calories)

// 	fmt.Printf("Max Calories: %v\n", max)

// 	topThree := TopThreeCalories(calories)

// 	fmt.Printf("Top Calories: %v\n", aoc_utils.SumInt(topThree))
// }
