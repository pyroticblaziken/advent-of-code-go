package main

import (
	"strconv"
	"strings"
)

func iterateCycle(c *int, x *int) int {
	*c++

	if *c%40 == 20 {
		return *c * *x
	}

	return 0
}

func ProcessInstructions(input []string) []int {
	cycle, iter, s, x := 0, 0, 0, 1
	signals := []int{}

	for iter = 0; iter < len(input); iter++ {
		if input[iter] == "noop" {
			s = iterateCycle(&cycle, &x)
			if s != 0 {
				signals = append(signals, s)
			}
		} else {
			parts := strings.Split(input[iter], " ")
			n, nErr := strconv.Atoi(parts[1])

			if nErr == nil {
				s = iterateCycle(&cycle, &x)
				if s != 0 {
					signals = append(signals, s)
				}
				s = iterateCycle(&cycle, &x)
				if s != 0 {
					signals = append(signals, s)
				}
				x += n
			}
		}
	}

	return signals
}

func updateDrawing(c *int, x int, l *[]string) {
	index := *c % 40
	row := *c / 40
	if index >= x-1 && index <= x+1 {
		(*l)[row] = (*l)[row][:index] + "#" + (*l)[row][index+1:]
	}
	*c++
}

func DrawScreen(instructions []string) string {
	cycle, x := 0, 1
	output := ""
	lines := []string{
		"........................................",
		"........................................",
		"........................................",
		"........................................",
		"........................................",
		"........................................",
	}

	for _, i := range instructions {
		if i == "noop" {
			updateDrawing(&cycle, x, &lines)
		} else {
			parts := strings.Split(i, " ")
			n, nErr := strconv.Atoi(parts[1])

			if nErr == nil {
				updateDrawing(&cycle, x, &lines)
				updateDrawing(&cycle, x, &lines)
				x += n
			}
		}
	}

	// Generate Output
	for i, l := range lines {
		output += l
		if i < len(lines)-1 {
			output += "\n"
		}
	}

	return output
}

// main.go
// func main() {
// 	input := aoc_utils.ReadInputFile("data/day10.txt")
// 	signals := ProcessInstructions(input)

// 	fmt.Printf("Signals: %v\n", aoc_utils.SumInt(signals[:6]))

// 	fmt.Println("Screen:")
// 	fmt.Println(DrawScreen(input))
// }
