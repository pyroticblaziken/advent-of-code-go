package main

import (
	"testing"

	"platinumleo.net/aoc_utils"
)

func TestCountVisibleTrees(t *testing.T) {
	input := aoc_utils.ReadInputFile("test_data/day08.txt")
	expected := 21
	trees := InputToTreeGrid(input)
	visible := GetVisibleTrees(trees)
	actual := len(visible)

	if expected != actual {
		t.Fatalf(`CountVisibleTrees(input) = %v, expected %v`, actual, expected)
	}
}

func TestGetHighestScenicScore(t *testing.T) {
	input := aoc_utils.ReadInputFile("test_data/day08.txt")
	expected := 8
	trees := InputToTreeGrid(input)
	actual := GetHighestScenicScore(trees)

	if expected != actual {
		t.Fatalf(`GetHighestScenicScore(input) = %v, expected %v`, actual, expected)
	}
}
