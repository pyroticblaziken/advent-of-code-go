package main

import "strings"

type RPSChoice int64

const (
	Rock     RPSChoice = 1
	Paper              = 2
	Scissors           = 3
)

func StringToRPSChoice(value string) RPSChoice {
	if value == "B" || value == "Y" {
		return Paper
	}
	if value == "C" || value == "Z" {
		return Scissors
	}

	return Rock
}

func CalculateMatchScore(elf RPSChoice, me RPSChoice) int {
	score := 0

	score += int(me)

	if elf == me {
		score += 3 // Draw: 3
	} else if (elf == Rock && me == Paper) || (elf == Paper && me == Scissors) || (elf == Scissors && me == Rock) { // I win
		score += 6 // Win: 6
	} else {
		score += 0 // Loss: 0
	}

	return score
}

func CalculateRPSScore(matches []string) int {
	score := 0

	for _, m := range matches {
		if len(m) == 3 {
			parts := strings.Split(m, " ")
			elf := StringToRPSChoice(parts[0])
			me := StringToRPSChoice(parts[1])
			score += CalculateMatchScore(elf, me)
		}
	}

	return score
}

func RigMatches(a string, b string) (RPSChoice, RPSChoice) {
	elf := StringToRPSChoice(a)
	me := Rock

	if b == "Y" { // Draw
		me = elf
	} else if b == "X" { // Lose
		switch elf {
		case Rock:
			me = Scissors
		case Paper:
			me = Rock
		default:
			me = Paper
		}
	} else { // Win
		switch elf {
		case Rock:
			me = Paper
		case Paper:
			me = Scissors
		default:
			me = Rock
		}
	}

	return elf, me
}

func CalculateTournamentScore(matches []string) int {
	score := 0

	for _, m := range matches {
		parts := strings.Split(m, " ")
		elf, me := RigMatches(parts[0], parts[1])
		score += CalculateMatchScore(elf, me)
	}

	return score
}

// main.go
// func main() {
// 	input := aoc_utils.ReadInputFile("data/day02.txt")

// 	score := CalculateRPSScore(input)

// 	fmt.Printf("RPS Score: %v\n", score)

// 	tScore := CalculateTournamentScore(input)

// 	fmt.Printf("Tournament Score: %v\n", tScore)
// }
