package main

import "testing"

var day03TestInput = []string{
	"vJrwpWtwJgWrhcsFMMfFFhFp",
	"jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
	"PmmdzqPrVvPwwTWBwg",
	"wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn",
	"ttgJtRGJQctTZtZT",
	"CrZsJsPPZsGzwwsLwLmpwMDw",
}

func TestGetDoubledItemPriorityTotal(t *testing.T) {
	bags := day03TestInput
	expected := 157
	total := GetDoubledItemPriorityTotal(bags)

	if expected != total {
		t.Fatalf(`GetDoubledItemPriorityTotal(bags) = %v, expected %v`, total, expected)
	}
}

func TestGetBadgePriorities(t *testing.T) {
	bags := day03TestInput
	expected := 70
	total := GetBadgePriorities(bags)

	if expected != total {
		t.Fatalf(`GetBadgePriorities(bags) = %v, expected %v`, total, expected)
	}
}
