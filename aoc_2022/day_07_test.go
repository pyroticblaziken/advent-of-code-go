package main

import (
	"testing"

	"platinumleo.net/aoc_utils"
)

func TestTotalSize(t *testing.T) {
	input := aoc_utils.ReadInputFile("test_data/day07.txt")
	expected := 48381165
	files := BuildTree(input)
	actual := files.TotalSize()

	if expected != actual {
		t.Fatalf(`TotalSize(input) = %v, expected %v`, actual, expected)
	}
}

func TestUndersizedDirectories(t *testing.T) {
	input := aoc_utils.ReadInputFile("test_data/day07.txt")
	expected := 95437
	files := BuildTree(input)
	actual := files.UndersizedDirectories()

	if expected != actual {
		t.Fatalf(`UndersizedDirectories(input) = %v, expected %v`, actual, expected)
	}
}

func TestSmallestToUpdate(t *testing.T) {
	input := aoc_utils.ReadInputFile("test_data/day07.txt")
	expected := 24933642
	files := BuildTree(input)
	actual := files.SmallestToUpdate()

	if expected != actual {
		t.Fatalf(`SmallestToUpdate(input) = %v, expected %v`, actual, expected)
	}
}
