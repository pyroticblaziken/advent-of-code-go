package main

import (
	"reflect"
	"testing"
)

var testValue = []string{
	"1000",
	"2000",
	"3000",
	"",
	"4000",
	"",
	"5000",
	"6000",
	"",
	"7000",
	"8000",
	"9000",
	"",
	"10000",
}

func TestCountCalories(t *testing.T) {
	input := testValue
	want := []int{6000, 4000, 11000, 24000, 10000}

	calories := CountCalories(input)

	if !reflect.DeepEqual(want, calories) {
		t.Fatalf(`CountCalories([input]) = %q, %v, want match for %#q, nil`, calories, nil, want)
	}
}

func TestMaxCalories(t *testing.T) {
	input := []int{6000, 4000, 11000, 24000, 10000}
	want := 24000

	maxC := MaxCalories(input)

	if maxC != want {
		t.Fatalf(`MaxCalories([input]) = %q, want match for %q`, maxC, want)
	}
}

func TestTopThreeCalories(t *testing.T) {
	input := []int{6000, 4000, 11000, 24000, 10000}
	want := []int{24000, 11000, 10000}

	topThree := TopThreeCalories(input)

	if !reflect.DeepEqual(want, topThree) {
		t.Fatalf(`TopThreeCalories([input]) = %v, %v`, topThree, want)
	}
}
