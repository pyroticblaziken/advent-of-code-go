package main

import (
	"testing"
)

func TestGetMarkerIndex(t *testing.T) {
	input := "mjqjpqmgbljsphdztnvjfqwrcgsmlb"
	expected := 6
	actual := GetMarkerIndex(input, 4)

	if expected != actual {
		t.Fatalf(`GetMarkerIndex(input) = %v, expected %v`, actual, expected)
	}
}

func TestGetMarkerIndexLong(t *testing.T) {
	input := "mjqjpqmgbljsphdztnvjfqwrcgsmlb"
	expected := 18
	actual := GetMarkerIndex(input, 14)

	if expected != actual {
		t.Fatalf(`GetMarkerIndex(input) = %v, expected %v`, actual, expected)
	}
}
