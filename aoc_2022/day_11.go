package main

import (
	"fmt"
	"math/big"
	"regexp"
	"strconv"
	"strings"

	"platinumleo.net/aoc_utils"
)

type Int = big.Int

type Monkey struct {
	Id      int
	Items   []*Int
	Op      string
	FactorA string
	FactorB string
	Divisor *Int
	Valid   int
	Invalid int
}

var idRegex = regexp.MustCompile(`Monkey (?P<Id>\d+):$`)

func inputToMonkeys(input []string) []Monkey {
	monkeys := []Monkey{}

	for i := 0; i < len(input); i++ {
		m := Monkey{}

		// Get Id
		idValues := idRegex.FindStringSubmatch(input[i])
		id, idErr := strconv.Atoi(idValues[1])
		if idErr == nil {
			m.Id = id
		}
		i++

		// Get Items
		itemParts := strings.Split(input[i], ": ")
		itemNumbers := strings.Split(itemParts[1], ", ")
		for _, n := range itemNumbers {
			l, lErr := strconv.Atoi(n)
			if lErr == nil {
				item := big.NewInt(int64(l))
				m.Items = append(m.Items, item)
			}
		}
		i++

		// Get Op and Factor
		opParts := strings.Split(input[i], "new = ")
		opPieces := strings.Split(opParts[1], " ")
		m.FactorA = opPieces[0]
		m.Op = opPieces[1]
		m.FactorB = opPieces[2]
		i++

		// Get Divisor
		divParts := strings.Split(input[i], "by ")
		d, dErr := strconv.ParseUint(divParts[1], 10, 64)
		if dErr == nil {
			m.Divisor = big.NewInt(int64(d))
		}
		i++

		// Get Valid
		validParts := strings.Split(input[i], "monkey ")
		v, vErr := strconv.Atoi(validParts[1])
		if vErr == nil {
			m.Valid = v
		}
		i++

		// Get Invalid
		invalidParts := strings.Split(input[i], "monkey ")
		v, vErr = strconv.Atoi(invalidParts[1])
		if vErr == nil {
			m.Invalid = v
		}
		i++

		monkeys = append(monkeys, m)
	}

	return monkeys
}

func inspectItem(item *Int, monkey Monkey) *Int {
	var v1, v2 *Int

	if monkey.FactorA == "old" {
		v1 = item
	} else {
		v, _ := strconv.ParseInt(monkey.FactorA, 10, 64)
		v1 = big.NewInt(v)
	}

	if monkey.FactorB == "old" {
		v2 = item
	} else {
		v, _ := strconv.ParseInt(monkey.FactorB, 10, 64)
		v2 = big.NewInt(v)
	}

	if monkey.Op == "+" { // Addition
		return item.Add(v1, v2)
	} else { // Multiplication
		return item.Mul(v1, v2)
	}
}

var bigZero = big.NewInt(0)
var bigThree = big.NewInt(3)

func MonkeyBusiness(input []string, rounds int, relief bool) int {
	mb := 1
	monkeys := inputToMonkeys(input)
	inspections := []int{}

	fmt.Println(monkeys)

	for k := 0; k < len(monkeys); k++ {
		inspections = append(inspections, 0)
	}

	for r := 0; r < rounds; r++ {
		fmt.Printf("Round %v\n", r+1)
		for m := 0; m < len(monkeys); m++ {
			for ok := len(monkeys[m].Items) > 0; ok; ok = len(monkeys[m].Items) > 0 {
				item := monkeys[m].Items[0]
				monkeys[m].Items = monkeys[m].Items[1:]
				item = inspectItem(item, monkeys[m])
				if relief {
					item = item.Div(item, bigThree)
				}
				inspections[m]++
				test := big.NewInt(item.Int64())
				mod := big.NewInt(0)
				test.DivMod(test, monkeys[m].Divisor, mod)
				if test == bigZero {
					monkeys[monkeys[m].Valid].Items = append(monkeys[monkeys[m].Valid].Items, item)
				} else {
					monkeys[monkeys[m].Invalid].Items = append(monkeys[monkeys[m].Invalid].Items, item)
				}
			}
		}

		for _, m := range monkeys {
			fmt.Printf("Monkey %v: %v\n", m.Id, m.Items)
		}
		fmt.Println()
	}

	i := aoc_utils.MaxIndex(inspections)
	mb *= inspections[i]
	inspections = aoc_utils.RemoveAt(inspections, i)
	i = aoc_utils.MaxIndex(inspections)
	mb *= inspections[i]

	return mb
}
