package main

import "testing"

var testInput = []string{
	"A Y",
	"B X",
	"C Z",
}

func TestCalculateRPSScore(t *testing.T) {
	input := testInput
	want := 15

	score := CalculateRPSScore(input)

	if score != want {
		t.Fatalf(`CalculateRPSScore([input]) = %v, wanted %v`, score, want)
	}
}

func TestCalculateTournamentScore(t *testing.T) {
	input := testInput
	want := 12

	score := CalculateTournamentScore(input)

	if score != want {
		t.Fatalf(`CalculateTournamentScore([input]) = %v, wanted %v`, score, want)
	}
}
