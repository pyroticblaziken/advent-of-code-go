package main

import (
	"platinumleo.net/aoc_utils"
)

func GetPriority(char rune) int {
	if char >= 97 {
		return int(char - 96)
	} else if char >= 65 {
		return int(char - 38)
	}
	return 0
}

func GetDoubledItem(bag string) rune {
	length := len(bag)
	first := bag[length/2:]
	second := bag[:length/2]

	for _, c := range first {
		if aoc_utils.StringContainsRune(second, c) {
			return c
		}
	}

	return 0
}

func GetDoubledItemPriorityTotal(bags []string) int {
	total := 0
	for _, b := range bags {
		r := GetDoubledItem(b)
		total += GetPriority(r)
	}

	return total
}

func GetCommonItems(a string, b string) string {
	output := []rune{}

	for _, i := range a {
		if aoc_utils.StringContainsRune(b, i) && !aoc_utils.Contains(output, i) {
			output = append(output, i)
		}
	}

	return string(output)
}

func GetBadges(bags []string) []string {
	badges := []string{}

	for i := 0; i < len(bags); i += 3 {
		first := bags[i]
		second := bags[i+1]
		third := bags[i+2]

		possibleBadges := GetCommonItems(first, second)
		badge := GetCommonItems(possibleBadges, third)
		badges = append(badges, badge)
	}

	return badges
}

func GetBadgePriorities(bags []string) int {
	priorities := 0

	badges := GetBadges(bags)

	for _, b := range badges {
		for _, i := range b {
			priorities += GetPriority(i)
		}
	}

	return priorities
}

// main.go
// func main() {
// 	input := aoc_utils.ReadInputFile("data/day03.txt")

// 	total := GetDoubledItemPriorityTotal(input)

// 	fmt.Printf("Priority Total: %v\n", total)

// 	badges := GetBadgePriorities(input)

// 	fmt.Printf("Badge Total: %v\n", badges)
// }
