package main

import (
	"strings"
)

func GetMarkerIndex(message string, length int) int {
	marker := -1

	for i := 0; i < len(message)-length+1; i++ {
		substring := message[i : i+length]
		for j := 0; j < length; j++ {
			test := string(substring[j])
			if strings.Index(substring, test) < j {
				break // No more testing needed we found a double
			}

			if j == length-1 {
				marker = i + j
			}
		}

		if marker != -1 {
			break // We found the number
		}
	}

	return marker
}

// main.go
// func main() {
// 	input := aoc_utils.ReadInputFile("data/day06.txt")

// 	messageIndex := GetMarkerIndex(input[0], 14)

// 	fmt.Printf("Marker Index: %v\n", messageIndex+1)
// }
