package main

import (
	"regexp"
	"strconv"
)

type FSEntry struct {
	Name     string
	Size     int
	Children []*FSEntry
	Parent   *FSEntry
}

var cdRegex = regexp.MustCompile(`^\$ cd (?P<Directory>.+)$`)

// var lsRegex = regexp.MustCompile(`^\$ ls$`)
var lsItemRegex = regexp.MustCompile(`^(dir|\d+) (.+)$`)

func BuildTree(commands []string) FSEntry {
	root := FSEntry{Name: "/", Size: 0}
	pwd := &root

	// Process commands
	for _, c := range commands {
		if cdRegex.MatchString(c) {
			values := cdRegex.FindStringSubmatch(c)
			if values[1] == ".." {
				pwd = pwd.Parent
			} else if values[1] == "/" {
				pwd = &root
			} else {
				for _, e := range pwd.Children {
					if values[1] == e.Name {
						pwd = e
						break
					}
				}
			}
		} else if lsItemRegex.MatchString(c) {
			values := lsItemRegex.FindStringSubmatch(c)
			entry := FSEntry{Name: values[2]}
			s, sErr := strconv.Atoi(values[1])
			if sErr == nil {
				entry.Size = s
			}
			entry.Parent = pwd
			pwd.Children = append(pwd.Children, &entry)
		}
	}

	return root
}

func (entry *FSEntry) TotalSize() int {
	totalSize := entry.Size

	for _, e := range entry.Children {
		totalSize += e.TotalSize()
	}

	return totalSize
}

func (entry *FSEntry) UndersizedDirectories() int {
	output := 0

	if entry.Size > 0 { // Skip Files
		return 0
	}

	size := entry.TotalSize()
	if size <= 100000 {
		output += size
	}

	for _, e := range entry.Children {
		output += e.UndersizedDirectories()
	}

	return output
}

func (entry *FSEntry) SmallestToUpdate() int {
	neededSpace := entry.TotalSize() - (70000000 - 30000000)
	smallestDir := int(^uint(0) >> 1)

	entry.SmallestHelper(&smallestDir, neededSpace)

	return smallestDir
}

func (entry *FSEntry) SmallestHelper(smallestDir *int, neededSpace int) {
	if entry.Size > 0 {
		return // Don't check files
	}
	potentialSize := entry.TotalSize()

	if potentialSize < *smallestDir && potentialSize > neededSpace {
		*smallestDir = potentialSize
	}

	for _, e := range entry.Children {
		e.SmallestHelper(smallestDir, neededSpace)
	}
}

// main.go
// func main() {
// 	input := aoc_utils.ReadInputFile("data/day07.txt")
// 	files := BuildTree(input)
// 	totalSize := files.UndersizedDirectories()

// 	fmt.Printf("Total Size: %v\n", totalSize)

// 	smallestDir := files.SmallestToUpdate()

// 	fmt.Printf("Smallest Dir: %v\n", smallestDir)
// }
