package main

import (
	"testing"

	"platinumleo.net/aoc_utils"
)

var day04TestData = aoc_utils.ReadInputFile("test_data/day04.txt")

func TestCountEncompassingSectors(t *testing.T) {
	expected := 2
	actual := CountEncompassingSectors(day04TestData)

	if expected != actual {
		t.Fatalf(`CountEncompassingSectors(input) = %v, expected %v`, actual, expected)
	}
}

func TestCountOverlappingSectors(t *testing.T) {
	expected := 4
	actual := CountOverlappingSectors(day04TestData)

	if expected != actual {
		t.Fatalf(`CountOverlappingSectors(input) = %v, expected %v`, actual, expected)
	}
}
