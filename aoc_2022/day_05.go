package main

import (
	"regexp"
	"strconv"

	"platinumleo.net/aoc_utils"
)

type CraneInstruction struct {
	Amount int
	From   int
	To     int
}

func CreateCraneStacks(input []string) ([][]string, []CraneInstruction) {
	stacks := [][]string{}
	instructions := []CraneInstruction{}

	stackRegex := regexp.MustCompile(`\D+`)
	instructionRegex := regexp.MustCompile(`move (?P<Amount>\d+) from (?P<From>\d+) to (?P<To>\d+)`)

	lineIndex := 0
	endOfStacks := false

	for endOfStacks = false; !endOfStacks; endOfStacks = !stackRegex.MatchString(input[lineIndex]) {
		for j := 1; j < len(input[lineIndex]); j += 4 {
			value := string(input[lineIndex][j])
			if value != " " && stackRegex.MatchString(value) {
				stackIndex := (j - 1) / 4 // Which Stack to add to
				for s := len(stacks); s <= stackIndex; s++ {
					stacks = append(stacks, []string{}) // Append an empty string array
				}
				stacks[stackIndex] = append([]string{value}, stacks[stackIndex]...)
			}
		}
		lineIndex++
	}

	lineIndex++

	for i := lineIndex; i < len(input); i++ {
		values := instructionRegex.FindStringSubmatch(input[i])
		if len(values) == 4 {
			a, aErr := strconv.Atoi(values[1])
			f, fErr := strconv.Atoi(values[2])
			t, tErr := strconv.Atoi(values[3])

			if aErr == nil && fErr == nil && tErr == nil {
				instructions = append(instructions, CraneInstruction{Amount: a, From: f - 1, To: t - 1})
			}
		}
	}

	return stacks, instructions
}

func ProcessStacks(stacks [][]string, instructions []CraneInstruction, singleMove bool) {
	for _, i := range instructions {
		moving := stacks[i.From][len(stacks[i.From])-i.Amount : len(stacks[i.From])]
		stacks[i.From] = stacks[i.From][0 : len(stacks[i.From])-i.Amount]
		if singleMove {
			moving = aoc_utils.Reverse(moving)
		}

		stacks[i.To] = append(stacks[i.To], moving...)
	}
}

// main.go
// func main() {
// 	input := aoc_utils.ReadInputFile("data/day05.txt")

// 	stacks, instructions := CreateCraneStacks(input)

// 	ProcessStacks(stacks, instructions, false)

// 	for _, s := range stacks {
// 		fmt.Println(s)
// 	}
// }
