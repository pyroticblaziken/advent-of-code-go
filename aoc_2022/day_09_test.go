package main

import (
	"testing"

	"platinumleo.net/aoc_utils"
)

func TestGetUniqueTailPositions(t *testing.T) {
	input := aoc_utils.ReadInputFile("test_data/day09.txt")
	expected := 13
	moves := GenerateMoves(input)
	points := GetUniqueTailPositions(moves)
	actual := len(points)

	if expected != actual {
		t.Fatalf(`GetUniqueTailPositions(input) = %v, expected %v`, actual, expected)
	}
}

func TestGetLongRopeTailPositions(t *testing.T) {
	input := aoc_utils.ReadInputFile("test_data/day09b.txt")
	expected := 36
	moves := GenerateMoves(input)
	points := GetLongRopeTailPositions(moves)
	actual := len(points)

	if expected != actual {
		t.Fatalf(`GetLongRopeTailPositions(input) = %v, expected %v`, actual, expected)
	}
}
