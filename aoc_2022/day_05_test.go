package main

import (
	"reflect"
	"testing"

	"platinumleo.net/aoc_utils"
)

func TestProcessStacks(t *testing.T) {
	input := aoc_utils.ReadInputFile("test_data/day05.txt")
	stacks, instructions := CreateCraneStacks(input)
	ProcessStacks(stacks, instructions, true)

	expected := [][]string{}
	expected = append(expected, []string{"C"})
	expected = append(expected, []string{"M"})
	expected = append(expected, []string{"P", "D", "N", "Z"})

	if !reflect.DeepEqual(expected, stacks) {
		t.Fatalf(`ProcessStacks(stacks, instructions) = %v, expected %v`, stacks, expected)
	}
}
