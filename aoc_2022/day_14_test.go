package main

import (
	"testing"

	"platinumleo.net/aoc_utils"
)

func TestSimulateSand(t *testing.T) {
	input := aoc_utils.ReadInputFile("test_data/day14.txt")
	expected := 24
	actual := SimulateSand(input)

	if expected != actual {
		t.Fatalf("SimulateSand(input) = %v, expected %v", actual, expected)
	}
}

func TestSimulateSandWithFloor(t *testing.T) {
	input := aoc_utils.ReadInputFile("test_data/day14.txt")
	expected := 93
	actual := SimulateSandWithFloor(input)

	if expected != actual {
		t.Fatalf("SimulateSandWithFloor(input) = %v, expected %v", actual, expected)
	}
}

func TestSimulateSandWithMap(t *testing.T) {
	input := aoc_utils.ReadInputFile("test_data/day14.txt")
	expected := 93
	actual := SimulateSandWithMap(input)

	if expected != actual {
		t.Fatalf("SimulateSandWithMap(input) = %v, expected %v", actual, expected)
	}
}
