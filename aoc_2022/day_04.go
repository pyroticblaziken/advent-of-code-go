package main

import (
	"strconv"
	"strings"
)

type sector struct {
	Start int
	End   int
}

/*
 * -1 => A contains B, 1 => B contains A, 0 neither contains
 */
func SectorsEncompass(a sector, b sector) int {
	if a.Start <= b.Start && a.End >= b.End {
		return -1
	}

	if b.Start <= a.Start && b.End >= a.End {
		return 1
	}

	return 0
}

func SectorsOverlap(a sector, b sector) bool {
	return ((b.Start <= a.Start && a.Start <= b.End) ||
		(b.Start <= a.End && a.End <= b.End) ||
		(a.Start <= b.Start && b.Start <= a.End) ||
		(a.Start <= b.End && b.End <= a.End))
}

func StringToSector(value string) (sector, error) {
	data := strings.Split(value, "-")

	s, s_err := strconv.Atoi(data[0])
	e, e_err := strconv.Atoi(data[1])

	if s_err != nil {
		return sector{Start: 0, End: 0}, s_err
	}

	if e_err != nil {
		return sector{Start: 0, End: 0}, e_err
	}

	return sector{Start: s, End: e}, nil
}

func CountEncompassingSectors(input []string) int {
	count := 0

	for _, l := range input {
		data := strings.Split(l, ",")
		a, a_err := StringToSector(data[0])
		b, b_err := StringToSector(data[1])

		if a_err == nil && b_err == nil {
			if SectorsEncompass(a, b) != 0 {
				count++
			}
		}
	}

	return count
}

func CountOverlappingSectors(input []string) int {
	count := 0

	for _, l := range input {
		data := strings.Split(l, ",")
		a, a_err := StringToSector(data[0])
		b, b_err := StringToSector(data[1])

		if a_err == nil && b_err == nil {
			if SectorsOverlap(a, b) {
				count++
			}
		}
	}

	return count
}

// main.go
// func main() {
// 	input := aoc_utils.ReadInputFile("data/day04.txt")

// 	total := CountEncompassingSectors(input)

// 	fmt.Printf("Encompassing Sectors: %v\n", total)

// 	total = CountOverlappingSectors(input)

// 	fmt.Printf("Overlapping Sectors: %v\n", total)
// }
