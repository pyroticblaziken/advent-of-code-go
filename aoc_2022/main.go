package main

import (
	"fmt"

	"platinumleo.net/aoc_utils"
)

func main() {
	input := aoc_utils.ReadInputFile("data/day14.txt")
	// grains := SimulateSand(input)

	// fmt.Printf("Grains of Sand: %v\n", grains)

	grains := SimulateSandWithFloor(input)

	fmt.Printf("Grains with floor: %v", grains)
}
