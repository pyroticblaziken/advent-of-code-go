package main

import (
	"strconv"
	"strings"

	"platinumleo.net/aoc_utils"
)

func GenerateMoves(input []string) []Coordinate {
	moves := []Coordinate{}

	for _, i := range input {
		parts := strings.Split(i, " ")
		n, nErr := strconv.Atoi(parts[1])

		if nErr == nil {
			for j := 0; j < n; j++ {
				if parts[0] == "R" {
					moves = append(moves, Coordinate{X: 1, Y: 0})
				} else if parts[0] == "L" {
					moves = append(moves, Coordinate{X: -1, Y: 0})
				} else if parts[0] == "U" {
					moves = append(moves, Coordinate{X: 0, Y: -1})
				} else if parts[0] == "D" {
					moves = append(moves, Coordinate{X: 0, Y: 1})
				}
			}
		}

	}

	return moves
}

func GetUniqueTailPositions(moves []Coordinate) []Coordinate {
	head := Coordinate{X: 0, Y: 0}
	tail := Coordinate{X: 0, Y: 0}
	coors := []Coordinate{tail}

	// Move the Head
	for _, m := range moves {
		head = head.Add(m)
		// Check if Adjacent
		if !head.IsAdjacent(tail) {
			// If not Adjacent, Move Tail
			diff := head.Subtract(tail)
			diff = diff.ToUnitVector()
			tail = tail.Add(diff)
			// If Tail moved, add coordinate to coors if unique
			if !aoc_utils.Contains(coors, tail) {
				coors = append(coors, tail)
			}
		}
	}

	return coors
}

func GetLongRopeTailPositions(moves []Coordinate) []Coordinate {
	knots := []Coordinate{
		{}, {}, {}, {}, {}, {}, {}, {}, {}, {},
	}
	points := []Coordinate{knots[9]}

	for _, m := range moves {
		knots[0] = knots[0].Add(m)
		for i := 1; i < len(knots); i++ {
			if !knots[i-1].IsAdjacent(knots[i]) {
				diff := knots[i-1].Subtract(knots[i])
				diff = diff.ToUnitVector()
				knots[i] = knots[i].Add(diff)
			}
		}

		if !aoc_utils.Contains(points, knots[9]) {
			points = append(points, knots[9])
		}
	}

	return points
}

// main.go
// func main() {
// 	input := aoc_utils.ReadInputFile("data/day09.txt")
// 	moves := GenerateMoves(input)
// 	points := GetUniqueTailPositions(moves)

// 	fmt.Printf("Unique Points: %v\n", len(points))

// 	points = GetLongRopeTailPositions(moves)

// 	fmt.Printf("Long Points: %v\n", len(points))
// }
