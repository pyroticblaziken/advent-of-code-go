package main

import (
	"strconv"

	"platinumleo.net/aoc_utils"
)

type Coordinate = aoc_utils.Coordinate

func InputToTreeGrid(input []string) [][]int {
	trees := [][]int{}

	for _, l := range input {
		row := []int{}

		for _, c := range l {
			n, nErr := strconv.Atoi(string(c))
			if nErr == nil {
				row = append(row, n)
			}
		}

		trees = append(trees, row)
	}

	return trees
}

func GetVisibleTrees(trees [][]int) []Coordinate {
	visible := []Coordinate{}
	rowMax := len(trees[0]) - 1
	colMax := len(trees) - 1

	for c := 0; c <= colMax; c++ {
		visible = append(visible, []Coordinate{{X: 0, Y: c}, {X: rowMax, Y: c}}...)
	}

	for r := 1; r < rowMax; r++ {
		visible = append(visible, []Coordinate{{X: r, Y: 0}, {X: r, Y: colMax}}...)
	}

	// Test Left
	treeMax := -1
	for r := 1; r < rowMax; r++ {
		treeMax = trees[r][0]
		for c := 1; c < colMax; c++ {
			coor := Coordinate{X: r, Y: c}
			if trees[r][c] > treeMax {
				treeMax = trees[r][c]
				if !aoc_utils.Contains(visible, coor) {
					visible = append(visible, coor)
				}
			}
		}
	}

	// Test Right
	for r := 1; r < rowMax; r++ {
		treeMax = trees[r][colMax]
		for c := colMax - 1; c > 0; c-- {
			coor := Coordinate{X: r, Y: c}
			if trees[r][c] > treeMax {
				treeMax = trees[r][c]
				if !aoc_utils.Contains(visible, coor) {
					visible = append(visible, coor)
				}
			}
		}
	}

	// Test Down
	for c := 1; c < colMax; c++ {
		treeMax = trees[0][c]
		for r := 1; r < rowMax; r++ {
			coor := Coordinate{X: r, Y: c}
			if trees[r][c] > treeMax {
				treeMax = trees[r][c]
				if !aoc_utils.Contains(visible, coor) {
					visible = append(visible, coor)
				}
			}
		}
	}

	// Test Up
	for c := 1; c < colMax; c++ {
		treeMax = trees[rowMax][c]
		for r := rowMax - 1; r > 0; r-- {
			coor := Coordinate{X: r, Y: c}
			if trees[r][c] > treeMax {
				treeMax = trees[r][c]
				if !aoc_utils.Contains(visible, coor) {
					visible = append(visible, coor)
				}
			}
		}
	}

	return visible
}

func GetHighestScenicScore(trees [][]int) int {
	scenic := 0
	rowMax := len(trees[0]) - 1
	colMax := len(trees) - 1

	for r := 1; r < rowMax; r++ {
		for c := 1; c < colMax; c++ {
			_scenic := GetScenicScore(trees, Coordinate{X: r, Y: c})
			if scenic < _scenic {
				scenic = _scenic
			}
		}
	}

	return scenic
}

func GetScenicScore(trees [][]int, base Coordinate) int {
	n, e, s, w := 0, 0, 0, 0
	rowMax := len(trees[0]) - 1
	colMax := len(trees) - 1

	// Test North
	for r := base.X - 1; r > -1; r-- {
		n++
		if r == 0 { // We've hit the edge of the forest
			break
		}

		if trees[r][base.Y] >= trees[base.X][base.Y] { // We can't see past this tree
			break
		}
	}

	// Test East
	for c := base.Y + 1; c <= rowMax; c++ {
		e++
		if c == colMax { // We've hit the edge of the forest
			break
		}

		if trees[base.X][c] >= trees[base.X][base.Y] {
			break
		}
	}

	// Test South
	for r := base.X + 1; r <= rowMax; r++ {
		s++
		if r == rowMax { // We've hit the edge of the forest
			break
		}

		if trees[r][base.Y] >= trees[base.X][base.Y] { // We can't see past this tree
			break
		}
	}

	// Test West
	for c := base.Y - 1; c > -1; c-- {
		w++
		if c == colMax { // We've hit the edge of the forest
			break
		}

		if trees[base.X][c] >= trees[base.X][base.Y] {
			break
		}
	}

	return n * e * s * w
}

// main.go
// func main() {
// 	input := aoc_utils.XeadInputFile("data/day08.txt")
// 	trees := InputToTreeGrid(input)
// 	visible := GetVisibleTrees(trees)

// 	fmt.Printf("Trees: %v\n", len(visible))

// 	fmt.Printf("Scenic: %v\n", GetHighestScenicScore(trees))
// }
