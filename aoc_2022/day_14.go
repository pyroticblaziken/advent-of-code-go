package main

import (
	"fmt"
	"strconv"
	"strings"

	"platinumleo.net/aoc_utils"
)

// Create all of the squares of rock
func buildTiles(input []string) []Coordinate {
	tiles := []Coordinate{}
	for _, line := range input {
		parts := strings.Split(line, " -> ")
		points := []Coordinate{}
		for _, p := range parts {
			v := strings.Split(p, ",")
			x, xErr := strconv.Atoi(v[0])
			y, yErr := strconv.Atoi(v[1])
			if xErr != nil || yErr != nil {
				return []Coordinate{}
			}

			points = append(points, Coordinate{X: x, Y: y})
		}

		// Take points and process
		for i := 0; i < len(points)-1; i++ {
			a, b := points[i], points[i+1]

			if a.X > b.X {
				for x := a.X; x >= b.X; x-- {
					t := Coordinate{X: x, Y: a.Y}
					if !aoc_utils.Contains(tiles, t) {
						tiles = append(tiles, t)
					}
				}
			}

			if a.X < b.X {
				for x := a.X; x <= b.X; x++ {
					t := Coordinate{X: x, Y: a.Y}
					if !aoc_utils.Contains(tiles, t) {
						tiles = append(tiles, t)
					}
				}
			}

			if a.Y > b.Y {
				for y := a.Y; y >= b.Y; y-- {
					t := Coordinate{X: a.X, Y: y}
					if !aoc_utils.Contains(tiles, t) {
						tiles = append(tiles, t)
					}
				}
			}

			if a.Y < b.Y {
				for y := a.Y; y <= b.Y; y++ {
					t := Coordinate{X: a.X, Y: y}
					if !aoc_utils.Contains(tiles, t) {
						tiles = append(tiles, t)
					}
				}
			}
		}
	}

	return tiles
}

func fallDestination(tiles []Coordinate, grain Coordinate, maxY int) (Coordinate, bool) {
	collision := Coordinate{X: grain.X, Y: maxY + 1}
	found := false

	for _, t := range tiles {
		if t.X == grain.X && t.Y < collision.Y {
			collision.Y = t.Y - 1
			found = true
		}
	}

	if collision.Y < 0 || collision.Y >= maxY {
		found = false
	}

	return collision, found
}

/* Move the sand down in the cave system
 *
 * bool => If the grain of sand landed
 * Coordinate => The last Coordinate being tracked of the grain
 */
// func moveSand(tiles []Coordinate, grain Coordinate, maxY int) (Coordinate, bool) {
// 	grain, collision := fallDestination(tiles, grain, maxY)

// 	if !collision {
// 		return grain, collision
// 	} else {
// 		// If not left, return test with left grain
// 		left := Coordinate{X: grain.X - 1, Y: grain.Y}
// 		if !aoc_utils.Contains(tiles, left) {
// 			return moveSand(tiles, left, maxY)
// 		}

// 		// If not right, return test with right grain
// 		right := Coordinate{X: grain.X + 1, Y: grain.Y}
// 		if !aoc_utils.Contains(tiles, right) {
// 			return moveSand(tiles, right, maxY)
// 		}

// 		return grain, collision
// 	}

// 	return grain, false
// }

// Attempt 2
// Do while and move down within the same function
func moveSand(tiles []Coordinate, grain Coordinate, maxY int) (Coordinate, bool) {
	if grain.Y > maxY {
		return grain, false
	}

	down := Coordinate{X: grain.X, Y: grain.Y + 1}
	left := Coordinate{X: grain.X - 1, Y: grain.Y + 1}
	right := Coordinate{X: grain.X + 1, Y: grain.Y + 1}

	// If can move down
	if !aoc_utils.Contains(tiles, down) { // If can move down
		return moveSand(tiles, down, maxY)
	} else if !aoc_utils.Contains(tiles, left) { // If can move left
		return moveSand(tiles, left, maxY)
	} else if !aoc_utils.Contains(tiles, right) {
		return moveSand(tiles, right, maxY)
	}

	return grain, true
}

func printCaveSystem(cave []Coordinate, sand []Coordinate) {
	minX, minY, maxX, maxY := aoc_utils.MaxInt, 0, 0, 0

	for _, c := range cave {
		if c.X < minX {
			minX = c.X
		}

		if c.X > maxX {
			maxX = c.X
		}

		if c.Y < minY {
			minY = c.Y
		}

		if c.Y > maxY {
			maxY = c.Y
		}
	}

	for _, c := range sand {
		if c.X < minX {
			minX = c.X
		}

		if c.X > maxX {
			maxX = c.X
		}

		if c.Y < minY {
			minY = c.Y
		}

		if c.Y > maxY {
			maxY = c.Y
		}
	}

	for y := minY; y <= maxY; y++ {
		for x := minX; x <= maxX; x++ {
			t := Coordinate{X: x, Y: y}
			if aoc_utils.Contains(cave, t) {
				fmt.Print("#")
			} else if aoc_utils.Contains(sand, t) {
				fmt.Print("o")
			} else {
				fmt.Print(".")
			}
		}
		fmt.Println()
	}
}

func SimulateSand(input []string) int {
	grains := 0 // grains of sand
	cave := buildTiles(input)
	// caveBase := make([]Coordinate, len(cave))
	// copy(caveBase, cave)
	maxY := 0

	for _, c := range cave {
		if c.Y > maxY {
			maxY = c.Y
		}
	}

	fmt.Printf("Number of rocks: %v\n", len(cave))
	fmt.Printf("Max Y: %v\n", maxY)

	for ok := true; ok; ok = ok {
		grain, ok := moveSand(cave, Coordinate{X: 500, Y: 0}, maxY)
		if ok {
			if grain.Y < 0 {
				fmt.Println("We made an oopsie!")
				break
			}
			cave = append(cave, grain)
			grains++
		} else {
			break
		}
	}

	// sand := []Coordinate{}
	// for _, c := range cave {
	// 	if !aoc_utils.Contains(caveBase, c) {
	// 		sand = append(sand, c)
	// 	}
	// }

	// printCaveSystem(cave, sand)

	return grains
}

// Move the sand down the pathways
// Coordinate => Resting place of the sand

func moveSandWithFloor(tiles []Coordinate, grain Coordinate, floor int) Coordinate {
	if grain.Y == floor {
		return grain // Grain landed on the floor
	}

	down := Coordinate{X: grain.X, Y: grain.Y + 1}
	left := Coordinate{X: grain.X - 1, Y: grain.Y + 1}
	right := Coordinate{X: grain.X + 1, Y: grain.Y + 1}

	// If can move down
	if !aoc_utils.Contains(tiles, down) { // If can move down
		return moveSandWithFloor(tiles, down, floor)
	} else if !aoc_utils.Contains(tiles, left) { // If can move left
		return moveSandWithFloor(tiles, left, floor)
	} else if !aoc_utils.Contains(tiles, right) {
		return moveSandWithFloor(tiles, right, floor)
	}

	return grain
}

func SimulateSandWithFloor(input []string) int {
	grains := 0
	cave := buildTiles(input)
	floor := 0

	for _, c := range cave {
		if c.Y > floor {
			floor = c.Y
		}
	}

	floor++

	for ok := true; ok; ok = ok {
		grain := moveSandWithFloor(cave, Coordinate{X: 500, Y: 0}, floor)
		cave = append(cave, grain)
		grains++
		if grain.X == 500 && grain.Y == 0 {
			break
		}
	}

	return grains
}

func dropGrain(caveMap map[int][]int, grain Coordinate, floor int) Coordinate {
	if grain.Y == floor { // Early skip
		return grain
	}
	start := grain.Y
	grain.Y = floor

	for _, y := range caveMap[grain.X] {
		if y < grain.Y && y > start {
			grain.Y = y - 1
		}
	}

	if grain.Y == floor { // Hit the Floor
		return grain
	}

	// Test Against Left Side
	_, lRow := caveMap[grain.X-1]
	if !lRow {
		caveMap[grain.X-1] = []int{}
	}

	if !aoc_utils.Contains(caveMap[grain.X-1], grain.Y+1) {
		grain.X--
		grain.Y++
		return dropGrain(caveMap, grain, floor)
	}

	// Test Against Right Side
	_, rRow := caveMap[grain.X+1]
	if !rRow {
		caveMap[grain.X+1] = []int{}
	}

	if !aoc_utils.Contains(caveMap[grain.X+1], grain.Y+1) {
		grain.X++
		grain.Y++
		return dropGrain(caveMap, grain, floor)
	}

	return grain
}

func SimulateSandWithMap(input []string) int {
	grains := 0
	floor := 0
	caveMap := make(map[int][]int)
	cave := buildTiles(input)
	toProcess := []Coordinate{}

	// Build Map
	for _, c := range cave {
		_, e := caveMap[c.X]
		if !e {
			caveMap[c.X] = []int{}
		}

		caveMap[c.X] = append(caveMap[c.X], c.Y)

		if c.Y > floor {
			floor = c.Y
		}
	}

	floor++
	// grain := dropGrain(caveMap, Coordinate{500, 0, 0}, floor)
	// fmt.Printf("Grain landed: %v\n", grain)

	grain := dropGrain(caveMap, Coordinate{500, 0, 0}, floor)
	toProcess = append(toProcess, grain)

	for ok := true; ok; ok = ok { // I hate that there are no while loops in golang
		p := toProcess[0]
		toProcess = toProcess[1:]
		// fmt.Printf("To Process: %v\n", p)
		// fmt.Printf("Remaining: %v\n", len(toProcess))
		_, pExists := caveMap[p.X]
		if !pExists {
			caveMap[p.X] = []int{}
		}

		if !aoc_utils.Contains(caveMap[p.X], p.Y) {
			caveMap[p.X] = append(caveMap[p.X], p.Y)
			grains++

			right := dropGrain(caveMap, Coordinate{p.X + 1, p.Y, 0}, floor)
			left := dropGrain(caveMap, Coordinate{p.X - 1, p.Y, 0}, floor)
			up := Coordinate{p.X, p.Y - 1, 0}

			toProcess = append(toProcess, right, left, up)

			// fmt.Printf("Right: %v\nLeft: %v\nUp: %v\n", right, left, up)
		}

		if len(toProcess) == 0 || aoc_utils.Contains(toProcess, Coordinate{500, 0, 0}) {
			break
		}
	}

	fmt.Println(aoc_utils.IndexOf(toProcess, Coordinate{500, 0, 0}))

	for _, g := range toProcess {
		_, gExists := caveMap[g.X]
		if !gExists {
			caveMap[g.X] = []int{}
		}

		if !aoc_utils.Contains(caveMap[g.X], g.Y) {
			caveMap[g.X] = append(caveMap[g.X], g.Y)
			grains++
		}
	}

	return grains
}
