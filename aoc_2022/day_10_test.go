package main

import (
	"testing"

	"platinumleo.net/aoc_utils"
)

func TestProcessInstructions(t *testing.T) {
	input := aoc_utils.ReadInputFile("test_data/day10.txt")
	expected := 13140
	signals := ProcessInstructions(input)
	actual := aoc_utils.SumInt(signals)

	if expected != actual {
		t.Fatalf(`ProcessInstructions(input) = %v, expected %v`, actual, expected)
	}
}

func TestDrawScreen(t *testing.T) {
	input := aoc_utils.ReadInputFile("test_data/day10.txt")
	expected := `##..##..##..##..##..##..##..##..##..##..
###...###...###...###...###...###...###.
####....####....####....####....####....
#####.....#####.....#####.....#####.....
######......######......######......####
#######.......#######.......#######.....`
	actual := DrawScreen(input)
	if expected != actual {
		t.Fatalf("DrawScreen(input) failed...\nExpected:\n%v\n\nActual:\n%v\n", expected, actual)
	}
}
