package main

import (
	"fmt"
	"reflect"

	"platinumleo.net/aoc_utils"
)

func runeToHeight(r rune) int {
	if r == 'S' {
		return runeToHeight('a')
	}
	if r == 'E' {
		return runeToHeight('z')
	}

	return int(r) - 96
}

func generateMountain(input []string) ([]Coordinate, Coordinate, Coordinate) {
	mountain := []Coordinate{}
	start, end := Coordinate{}, Coordinate{}

	for y, line := range input {
		for x, r := range line {
			c := Coordinate{X: x, Y: y, Z: runeToHeight(r)}
			if r == 'S' {
				start = c
			} else if r == 'e' {
				end = c
			}
			mountain = append(mountain, c)
		}
	}

	return mountain, start, end
}

func nextCoordinates(mountain []Coordinate, point Coordinate) []Coordinate {
	nodes := []Coordinate{}

	upOk, up := aoc_utils.Find(mountain, func(c Coordinate) bool { return c.X == point.X && c.Y == point.Y-1 })
	downOk, down := aoc_utils.Find(mountain, func(c Coordinate) bool { return c.X == point.X && c.Y == point.Y+1 })
	leftOk, left := aoc_utils.Find(mountain, func(c Coordinate) bool { return c.X == point.X-1 && c.Y == point.Y })
	rightOk, right := aoc_utils.Find(mountain, func(c Coordinate) bool { return c.X == point.X+1 && c.Y == point.Y })

	if upOk && up.Z <= point.Z+1 {
		nodes = append(nodes, up)
	}

	if downOk && down.Z <= point.Z+1 {
		nodes = append(nodes, down)
	}

	if leftOk && left.Z <= point.Z+1 {
		nodes = append(nodes, left)
	}

	if rightOk && right.Z <= point.Z+1 {
		nodes = append(nodes, right)
	}

	return nodes
}

func coordinateScore(mountain []Coordinate, point Coordinate) int {
	return 1
}

func internalRemove(s []Coordinate, r Coordinate) []Coordinate {
	index := -1

	for i, c := range s {
		if reflect.DeepEqual(r, c) {
			index = i
		}
	}

	if index > -1 {
		return aoc_utils.RemoveAt(s, index)
	}

	return s
}

func my_a_star(start, end Coordinate, nodes []Coordinate) []Coordinate {
	path := []Coordinate{}

	openNodes := []Coordinate{start}
	cameFrom := make(map[string]Coordinate)

	gScore := make(map[string]int)
	gScore[start.ToString()] = 0

	fScore := make(map[string]int)
	fScore[start.ToString()] = start.ManhattanDistance(end)

	potentialWins := make(map[string]int)
	winMin := aoc_utils.MaxInt

	deadEndNodes := []Coordinate{}

	iterations := 0

	for ok := len(openNodes) > 0 && iterations < 1000; ok; ok = len(openNodes) > 0 && iterations < 1000 {
		var current Coordinate
		highScore := aoc_utils.MaxInt

		for _, o := range openNodes {
			if fScore[o.ToString()] < highScore {
				highScore, _ = fScore[o.ToString()]
				current = o
			}
		}

		openNodes = internalRemove(openNodes, current)

		if current == end {
			potentialWins[current.ToString()] = highScore
			if highScore < winMin {
				winMin = highScore
			}
			continue
		}

		if highScore > winMin {
			deadEndNodes = append(deadEndNodes, current)
		}

		if aoc_utils.Contains(deadEndNodes, current) {
			continue
		}

		nextNodes := nextCoordinates(nodes, current)

		if len(nextNodes) == 0 {
			deadEndNodes = append(deadEndNodes, current)
			continue
		}

		for _, n := range nextNodes {
			g, _ := gScore[current.ToString()]
			g += coordinateScore(nodes, n)
			gNext, gNextOk := gScore[n.ToString()]

			if !gNextOk || g < gNext {
				cameFrom[n.ToString()] = current
				gScore[n.ToString()] = g
				fScore[n.ToString()] = g + n.ManhattanDistance(end)
				openNodes = append(openNodes, n)
			}
		}

		iterations++
	}

	fmt.Printf("Iterations Run: %v\n", iterations)
	fmt.Printf("Open Nodes: %v\n", openNodes)
	fmt.Printf("Potential Paths: %v\n", potentialWins)
	fmt.Printf("Dead Ends: %v\n", deadEndNodes)

	return path
}

func FindPath(input []string) []Coordinate {
	mountain, start, end := generateMountain(input)

	return my_a_star(start, end, mountain)
}
