# Advent of Code - Go

## Quick Functions

`go run .` - Run main in go mod you are in
`go test` - Run Tests in go mod you are in

## Completed

- [x] [x] Day 01
- [x] [x] Day 02
- [x] [x] Day 03
- [x] [x] Day 04
- [x] [x] Day 05
- [x] [x] Day 06
- [x] [x] Day 07
- [x] [x] Day 08
- [x] [x] Day 09
- [x] [x] Day 10
- [x] [ ] Day 11
- [x] [x] Day 12
- [ ] [ ] Day 13
- [ ] [ ] Day 14
- [ ] [ ] Day 15
- [ ] [ ] Day 16
- [ ] [ ] Day 17
- [ ] [ ] Day 18
- [ ] [ ] Day 19
- [ ] [ ] Day 20
- [ ] [ ] Day 21
- [ ] [ ] Day 22
- [ ] [ ] Day 23
- [ ] [ ] Day 24
- [ ] [ ] Day 25
